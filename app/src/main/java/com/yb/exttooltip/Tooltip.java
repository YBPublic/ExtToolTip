package com.yb.exttooltip;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.Objects;

@SuppressLint("HandlerLeak")
public class Tooltip implements ViewTreeObserver.OnPreDrawListener {

    private PopupWindow popupWindow;
    private View contentView;
    private TextView text;
    private View anchor;

    private int left;
    private int right;
    private int bottom;

    private Tooltip(Context context, int textId, int textSize, int left, int right, int bottom, int color, int drawable) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = Objects.requireNonNull(inflater).inflate(R.layout.tooltip_layout, null);
        text = contentView.findViewById(R.id.text);

        text.setText(textId);
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(textSize));
        text.setTextColor(ContextCompat.getColor(context, color));
        text.setBackground(ContextCompat.getDrawable(context, drawable));

        this.left = (int) context.getResources().getDimension(left);
        this.right = (int) context.getResources().getDimension(right);
        this.bottom = (int) context.getResources().getDimension(bottom);
        contentView.setOnClickListener(v -> dismissTooltip());
    }

    private void show(final View anchor) {
        if (isTooltipShown()) {
            return;
        }

        this.anchor = anchor;

        contentView.getViewTreeObserver().addOnPreDrawListener(this);

        popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setContentView(contentView);
        popupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, 0);
    }

    private boolean isTooltipShown() {
        return popupWindow != null && popupWindow.isShowing();
    }

    private void dismissTooltip() {
        if (popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
    }

    @Override
    public boolean onPreDraw() {
        contentView.getViewTreeObserver().removeOnPreDrawListener(this);
        int location[] = new int[2];
        anchor.getLocationOnScreen(location);
        contentView.setPadding(left, location[1] - anchor.getPaddingTop() - text.getHeight() - bottom, right, 0);
        return false;
    }

    public static class Builder {
        private Context context;

        private int left = R.dimen.tooltip_padding_left;
        private int right = R.dimen.tooltip_padding_right;
        private int bottom = R.dimen.tooltip_padding_bottom;
        private int textSize = R.dimen.tooltip_text_size;
        private int drawable = R.drawable.tooltip_shape;
        private int color = R.color.white;
        private int text;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setPadding(@DimenRes int left, @DimenRes int right, @DimenRes int bottom) {
            this.left = left;
            this.right = right;
            this.bottom = bottom;
            return this;
        }

        public Builder setTextSize(@DimenRes int textSize) {
            this.textSize = textSize;
            return this;
        }

        public Builder setText(@StringRes int text) {
            this.text = text;
            return this;
        }

        public Builder setTextColor(@ColorRes int color) {
            this.color = color;
            return this;
        }

        public Builder setDrawable(@DrawableRes int drawable) {
            this.drawable = drawable;
            return this;
        }

        @UiThread
        public void show(View anchor) {
            new Tooltip(context, text, textSize, left, right, bottom, color, drawable).show(anchor);
        }
    }
}
